function findEvenandOdd() {
    var evennumber = "";
    var oddnumber = "";
    for (var i = 0; i < 100; i++) {
        if (i % 2 == 0) {
            evennumber += i + " ";
            oddnumber += i + 1 + " ";
            document.getElementById("resultb1").innerHTML = "Số chẵn: " + evennumber + "<br /> Số lẻ: " + oddnumber;
        }


    }
}

// /////////////////////////////////////////

function finddivisiblefor3() {
    var count = 0;
    for (var i = 0; i < 1000; i++) {
        if (i % 3 == 0) {
            count++;
            document.getElementById("resultb2").innerHTML = `Số chia hết cho 3 nhỏ hơn 1000: ${count} số`;
        }
    }
}

// /////////////////////////////////////////

function findsmallestinteger() {
    var integer = 0;
    var sum = 0;
    while (sum < 10000) {
        integer++;
        sum += integer;
    }

    document.getElementById("resultb3").innerHTML = `Số nguyên dương nhỏ nhất: ${integer}`
}


// ///////////////////////////////////

function findsum() {
    var numberX = document.getElementById("number-x").value * 1;
    var numberN = document.getElementById("number-n").value * 1;
    var sum = 0;
    for (var i = 1; i <= numberN; i++) {
        sum += Math.pow(numberX, i)
    }

    document.getElementById("resultb4").innerHTML = `Tổng: ${sum}`
}

// //////////////////////////////////////

function factorialcalculation() {
    var numberN = document.getElementById("txt-number-n").value * 1;
    var result = 1;
    for (var i = 1; i <= numberN; i++) {
        result = result * i;
    }
    document.getElementById("resultb5").innerHTML = `Giai thừa: ${result}`
}

// //////////////////////////////////////

function createDiv(){
    for(var i = 1; i <= 10; i++){
        if(i%2==0){
            document.getElementById("resultb6").innerHTML += `<div class="bg-danger"> Div chẵn </div>`
        }else{
            document.getElementById("resultb6").innerHTML += `<div class="bg-light"> Div lẻ </div>`
        }
    }
}